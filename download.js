const https = require('https');
const fs = require('fs');
const makeuuid = require("./uuid")
const rmdir = require("./rmdir")

if (fs.existsSync("./downloads")) {
    rmdir("./downloads")
}
fs.mkdirSync("./downloads")

function get(hostname, path) {
    let resolve
    let reject
    const promise = new Promise(function (res, rej) {
        resolve = res;
        reject = rej;
    })

    // const options = {
    //     hostname: hostname,
    //     port: 443,
    //     path: path,
    //     method: 'GET'
    // };

    // const req = https.request(options, (res) => {
    //     const uuid = makeuuid()
    //     console.log("Update Check Finished - Info:")
    //     console.log('statusCode:', res.statusCode);
    //     console.log('headers:', res.headers);
    //     console.log('Download UUID:', uuid);

    //     const file = fs.createWriteStream(`./downloads/DOWNLOAD_${uuid}.${hostname}.${path.split("/").join("-")}`);
    //     res.on('data', (d) => {
    //         d.pipe(file);
    //         file.end()
    //         // console.log(d.toString('utf8'))
    //         resolve(() => {
    //             return fs.readFileSync(`./downloads/DOWNLOAD_${uuid}.${hostname}.${path.split("/").join("-")}`)
    //         })
    //     });
    // });

    // req.on('error', (e) => {
    //     reject(e)
    //     throw new Error(e)
    // });
    // req.end();
    const uuid = makeuuid()
    const file = fs.createWriteStream(`./downloads/DOWNLOAD_${uuid}.${hostname}.${path.split("/").join("-")}`);
    https.get(`https://${hostname}${path}`, function (r) {
        r.pipe(file)
        file.on('finish', function () {
            file.close()
        });
        let a = () => {
            return new Promise((res, rej) => {
                const readStream = fs.createReadStream(a.filename);
                readStream.on('data', function (data) {
                    res(data)
                });
                readStream.on('error', function (err) {
                    rej(err)
                });
            })
        }
        a.uuid = uuid
        a.filename = `./downloads/DOWNLOAD_${uuid}.${hostname}.${path.split("/").join("-")}`
        a().then((d) => {
            a.rawContent=d.toString()
            a.toString=function(e){
                return a.rawContent.toString(e)
            }
            resolve(a)
        })
    })
    return promise;
}

module.exports = get;
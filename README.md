# YeetChat
## Client
This repository contains the client-source code for YeetChat.

## Dependencies
YeetChat Client currently depends on [Electron](https://electronjs.org), Chalk, and [Shared Functions](https://gitlab.com/yeetchat/yeetchat-shared-functions)*

\* made by the yeetchat team



## About YeetChat
YeetChat, heavily inspired by Discord, is an open-source, modifyable (at your own risk), program that has no ToS statements/clauses/subclauses against client modification.

YeetChat was originally thought of somewhere during the month of March, in the year 2020.

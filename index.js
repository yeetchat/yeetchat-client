/**
 * @name        YeetChat-Client/index.js
 * @description The main file for YeetChat (CLIENT)
 * 
 * @version     1.0.0
 * 
 * @author      0J3
 * @author      MorganDGamer
 */

// load shared functions
const {
    exists,
    hasArgument
} = require("shared_functions")

// custom error class
{
    // set old error to current error
    let oldError = Error

    // set custom error class - extends old error
    class CustomError extends oldError {
        constructor(str) {
            let finalstring = str


            // redefine finalstring to what it needs to be below this line
            let suffix = "\n\nSTACK TRACE:"
            let prefix = ""

            finalstring = prefix + finalstring + suffix
            // modified error code above this line


            return new oldError(finalstring)
        }
        oldConstructor = function (str) {
            return new oldError(str)
        }
    }

    // set error to customerror
    Error = CustomError
}

// argument thingy
if (hasArgument("--force_invalidenv_browser_error")) { // forces invalid environement as a browser, causing error
    document = true
    window = true
}


// ensure isnt running in browser environement
if ((exists(typeof (document)) && exists(typeof (window))) && !(hasArgument("--force_env") || hasArgument("--force"))) {
    throw new Error("Invalid Environement for index.js\n## Error: Invalid Environement: Index.JS should not ever run in a browser environement ##\nIt shouldn't even run in electron."); /**/ /**/ /**/
    return
}

// ensure is running in nodejs
if ((exists(typeof (document)) && exists(typeof (window))) && !(hasArgument("--force_node") || hasArgument("--force"))) {
    throw new Error("Invalid Environement for index.js\n## Error: Invalid Environement: Index.JS did not detect node running ##\n" + `module=${module}\nrequire=${require}.`); /**/ /**/ /**/
    return
}

// fs and yaml
const yaml = require('js-yaml');
const fs = require('fs');

// Ensure config exists
const defaultconf = `# Copyright (c) 2020 YeetChat. All Rights Reserved. #

address:
    protocol: https
    domain: yeetchat.xyz
loadingWindow:
    height: 850
    width: 1000`
if (!fs.existsSync(`${__dirname}/config.conf`)) {
    fs.writeFileSync(
        `${__dirname}/config.conf`,
        defaultconf + "\n"
    )
}

let conf

let version

// Get config, or throw exception on error
try {
    const doc = yaml.safeLoad(fs.readFileSync(`${__dirname}/config.conf`, 'utf8'));
    conf = doc
} catch (e) {
    throw new Error(e);
}

// Get version, or throw exception on error
try {
    const doc = yaml.safeLoad(fs.readFileSync(`${__dirname}/version.yml`, 'utf8'));
    version = doc
} catch (e) {
    throw new Error(e);
}



const {
    address
} = conf
const {
    protocol,
    domain
} = address

// run electon
const {
    app,
    BrowserWindow
} = require('electron');

const download = require("./download")

let mainWindow;

app.on('ready', () => {
    mainWindow = new BrowserWindow({
        show: false,
        frame: false,
        backgroundColor: "#2A2B2C",
        webPreferences: {
            nodeIntegration: true
        },
        width: conf.loadingWindow.width,
        height: conf.loadingWindow.height
    })

    mainWindow.once('show', () => {
        mainWindow.loadURL(`file://${__dirname}/localhtml/load/load.html#Checking for Updates... (Downloading version.yml)`)
        const promise = download("gitlab.com","/yeetchat/yeetchat-client/-/raw/master/version.yml")
        promise.then((d)=>{
            mainWindow.loadURL(`file://${__dirname}/localhtml/load/load.html#Checking for Updates... (Parsing YML)`)

            let latestversion
            try {
                d=d.rawContent
                const doc = yaml.safeLoad(d, 'utf8');
                latestversion = doc
            } catch (e) {
                mainWindow.loadURL(`file://${__dirname}/localhtml/load/load.html#Error`)
                throw new Error(e);
            }

            mainWindow.loadURL(`file://${__dirname}/localhtml/load/load.html#Checking for Updates... (Comparing downloaded version.yml to local one)`)

            if (version.build<latestversion.build && !hasArgument("--force")) {
                mainWindow.loadURL(`file://${__dirname}/localhtml/load/load.html#An update is available... Downloading Installer...`)
            } else if (version.build>latestversion.build && !hasArgument("--force")) {
                mainWindow.loadURL(`file://${__dirname}/localhtml/load/load.html#A downgrade is required. Please install the latest version manually. (Specify --force while starting to bypass this)`)
            } else {
                mainWindow.loadURL(`file://${__dirname}/localhtml/load/load.html#Running latest version! Starting...`)
                setTimeout(()=>{
                    mainWindow.loadURL(`${protocol}://${domain}/app/login`)
                },250)
            }

        })
        promise.catch((e)=>{
            mainWindow.loadURL(`file://${__dirname}/localhtml/load/load.html#Error`)
            try {
                mainWindow.close()
            } catch (error) {
                
            }
            throw new Error(e)
        })
    })
    mainWindow.loadURL(`file://${__dirname}/localhtml/load/load.html#Initializing Window`)
    mainWindow.show()
})
// app.on('ready', () => {
//   mainWindow = new BrowserWindow({
//       height: 600,
//       width: 800, 
//       webPreferences: {
//           nodeIntegration: true
//       },
//       backgroundColor: "#2A2B2C"
//   });
//   mainWindow.loadFile('index.html');
// });